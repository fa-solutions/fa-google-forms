let FAClient = null;
let notyf = null;
let notificationOpen = false;

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL2ZhLWdvb2dsZS1mb3Jtcw==`,
};


function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    FAClient.on('openForm', ({url}) => {
        document.getElementById('frameContainer').innerHTML = '';
        const iFrame = document.createElement('iframe');
        iFrame.src = url;
        iFrame.style = 'width: 100vw; min-width: 100vw;min-height: 100vh; height: 100vh; border: none;';
        document.getElementById('frameContainer').appendChild(iFrame);
        FAClient.open();
    });
}

